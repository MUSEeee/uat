class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        print(self.name)


class Cat(Animal):
    def mau(self):
        print("MAU MAU")


class Dog(Animal):
    def __init__(self, lastname, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lastname = lastname

    def gav(self):
        print('GAV, GAV')


cat = Cat('Bob', 2)
print(cat.name)
cat.mau()

dog = Dog('Bobik', 2, 'tvar')
print(dog.name)
print(dog.lastname)
dog.gav()

Cat('KON', 3).get_name()


