import time

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By


def make_screenshot_if_fails(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            args[0].browser.save_screenshot('1.png')
            raise e
    return wrapper


def click_and_send_keys(context, locator, value):
    context.browser.find_element_by_xpath(locator).click()
    time.sleep(1.5)
    context.browser.find_element_by_xpath(locator).send_keys(value)
    time.sleep(1.5)


def get_memory(cursor, sql_request):
    cursor.execute(sql_request)
    return cursor.fetchall()[-1][0]


def select_program(context, locator):
    time.sleep(0.5)
    context.browser.find_element_by_xpath(locator).click()


def customer_failure(context, reason='Другая причина'):
    try:
        WebDriverWait(context.browser, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[text()[contains(.,'Проставить отказ клиента')]]")))
    except TimeoutException:
        print("Не найдена кликабельная кнопка 'Проставить отказ клиента'")
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Проставить отказ клиента')]]").click()
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Причина отказа']").send_keys(reason + Keys.RETURN)
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, "//*[text()[contains(.,'Сохранить')]]")))
        context.browser.execute_script("arguments[0].scrollIntoView();", context.browser.find_element_by_xpath("//*[text()[contains(.,'Сохранить')]]"))
        WebDriverWait(context.browser, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[text()[contains(.,'Сохранить')]]")))
    except TimeoutException:
        print("Не найдена кликабельная кнопка 'Сохранить' после выбора причины отказа")
    save_btn = context.browser.find_element_by_xpath("//*[text()[contains(.,'Сохранить')]]")
    save_btn.click()
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, "//*[text()='Заявка закрыта']")))
        print("Заявка закрыта с причиной - ", reason)
    except TimeoutException:
        print("Нет сообщения об отказе клиента")
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, "//*[text()[contains(.,'Закрыть')]]")))
        context.browser.execute_script("arguments[0].scrollIntoView();", context.browser.find_element_by_xpath("//*[text()[contains(.,'Закрыть')]]"))
        WebDriverWait(context.browser, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[text()[contains(.,'Закрыть')]]")))
    except TimeoutException:
        print("Нет кликабельной кнопки закрытия сообщения об отказе")
    close_btn = context.browser.find_element_by_xpath("//*[text()[contains(.,'Закрыть')]]")
    close_btn.click()


def is_element_present(context, how, what, timeout=4):
    try:
         WebDriverWait(context.browser, timeout).until(EC.presence_of_element_located((how, what)))
    except TimeoutException:
        return False
    return True