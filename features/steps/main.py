import shutil
import threading
import time as t
import requests
from behave import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from common import *
from const import *
from behave.__main__ import main as behave_main


@step(u'run in parallel "{feature}" "{scenario}"')
def step_impl(context, feature, scenario):
    t = threading.Thread(
        name='run test parallel',
        target=parallel_executor,
        args=[context, feature, scenario])
    # args=[context, 'parallel_actions.feature', 'Make Cab-Cab communication'])
    t.start()


def parallel_executor(context, feature_name, scenario):
    os.chdir(r'C:/Users/kotov/PycharmProjects/pythonProject')
    behave_main('-i "{}" -n "{}" --no-capture --no-skipped'.format(feature_name, scenario))


@Given('Переходим в партнерку "{url}" и проходим авторизацию "{login}" и "{password}"')
def step(context, url, login, password):
    options = webdriver.ChromeOptions()
    options.add_argument('--window-size=1920x1080')  # for docker
    options.add_argument('--headless') #for docker
    options.add_argument('--no-sandbox') #for docker
    options.add_argument('--disable-dev-shm-usage') #for docker
    # options.add_argument("--disable-infobars")
    # options.add_argument("--disable-notifications")
    # options.add_argument("--incognito")
    options.add_argument('--ignore-certificate-errors') #for docker
    options.add_argument('--allow-insecure-localhost') #for docker
    # desired_capabilities = {
    #     "acceptInsecureCerts": True
    # }
    # for docker '/usr/local/bin/chromedriver'
    # for local 'C:/Users/kotov/Desktop/chromedriver.exe'
    context.browser = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=options)
    context.browser.get(url)
    context.browser.maximize_window()
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable((By.NAME, 'login'))
    )
    context.browser.find_element_by_name('login').send_keys(login)
    context.browser.find_element_by_name('password').send_keys(password)
    context.browser.find_element_by_class_name('BpButton__wrap').find_element_by_xpath('..').click()


@Then('Дожидаемся появления кнопки "{text}"')
def step(context, text):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable((By.XPATH, "//a[@class='BpButton MenuRequestsManager__buttonNew  -disabled-no "
                                              "-active-no -focus-no -size-large -skin-default -wait-no']"))
    )
    context.browser.save_screenshot('screenNEW1.png')


@Then('Создаём заявку для "{client}"')
def step(context, client):
    context.browser.save_screenshot('screenNEW2.png')
    context.browser.find_element_by_xpath("//a[@class='BpButton MenuRequestsManager__buttonNew  -disabled-no "
                                          "-active-no -focus-no -size-large -skin-default -wait-no']").click()
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located(
            (By.XPATH, "//*[text()[contains(.,'Общая информация')]]"))

    )

    var = context.browser.current_url.split("/")[4]
    os.environ["id"] = var
    print(os.environ["id"])

    context.browser.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(
        CLIENTS.get(client).get('lastName') + Keys.ENTER)
    context.browser.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(
        CLIENTS.get(client).get('firstName') + Keys.ENTER)
    context.browser.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(
        CLIENTS.get(client).get('secondName') + Keys.ENTER)
    context.browser.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(random_phone())
    context.browser.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys("kotov@balance-pl.com")
    context.browser.find_element_by_xpath("//input[@data-label='Стаж на текущем месте']").click()
    t.sleep(1)
    context.browser.find_element_by_xpath('//div[contains(text(), "Более 5 лет")]').click()
    form_passport = context.browser.find_elements_by_xpath('//input[contains(@class, "BpUpload__input")]')
    form_passport[0].send_keys(os.path.join(cwd, 'pythonProject/features/docs', '7pass_Ch.jpg'))
    form_passport[1].send_keys(os.path.join(cwd, 'pythonProject/features/docs', '6pass_Ch.jpg'))

    context.browser.find_element_by_xpath("//button[@class='BpButtonNext PageRequestStep01__buttonNext PageRequestStep0"
                                          "1__buttonNext--deviceMode-desktop -disabled-no -active-no -focus-no "
                                          "-wait-no']").click()

    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located(
            (By.XPATH, "//*[text()[contains(.,'Проверка документов')]]")))


# TODO ======= 2 step =======


@Then('Возвращаемся в партнерку')
def step(context):
    context.browser.switch_to.window(context.browser.window_handles[0])
    context.browser.refresh()
    t.sleep(2)


@Then('Прикладываем паспорт')
def step(context):
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located(
            (By.XPATH, "//*[text()[contains(.,'Паспортные данные')]]")))
    t.sleep(2)
    form_passport = context.browser.find_elements_by_xpath('//input[contains(@class, "BpUpload__input")]')
    form_passport[0].send_keys(allF)


@Then('Скачиваем и отправляем согласие - "{agreement}"')
def step(context, agreement):
    if agreement == "из системы":
        doc = context.browser.find_element_by_xpath(
            "//*[text()[contains(.,'Распечатать')]]").get_attribute('href')
        with open(os.path.join(cwd, 'pythonProject/features/docs', 'sogl.pdf')) as f:
            shutil.copyfileobj(requests.get(doc, stream=True, verify=False).raw, f)
        agreement = os.path.join(cwd, 'pythonProject/features/docs', 'sogl.pdf')
    try:
        context.browser.find_elements_by_class_name('BpUpload__input')[1].send_keys(f'{agreement}')
    except:
        context.browser.find_elements_by_class_name('BpUpload__input')[1].send_keys(
            os.path.join(cwd, 'pythonProject/features/docs', 'super_PTS.pdf'))
    t.sleep(5)
    context.browser.find_element_by_class_name('BpButtonNext__wrap').click()


@Then('Ожидаем появления раздела "{section}"')
def step(context, section):
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located(
            (By.XPATH, f"//*[text()[contains(.,'{section}')]]")))
    t.sleep(0.5)


@Then('Заполняем работу и занятость "{job}" - "{money}" - "{experience}"')
@make_screenshot_if_fails
def step(context, job, money, experience):
    """
    :param context:
    :param money: доход в месяц
    :param job: тип занятости (Работа по найму/Индивидуальный предприниматель/Пенсионер/Военнослужащий/
    Частная практика - адвокат/Частная практика - нотариус/Собственный бизнес)
    :param experience: стаж на текущем месте (До 3 месяцев/От 3 до 6 месяцев/От 6 до 12 месяцев/Больше года)
    """
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable((By.XPATH, "//*[text()[contains(.,'Тип занятости')]]"))
    )
    t.sleep(1)
    click_and_send_keys(context, ".//INPUT[@data-label='Тип занятости']", f'{job}' + Keys.ENTER)
    t.sleep(1)

    if 'Работа по найму' in job:
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Фактический адрес')]", 'г Москва')
        t.sleep(1)
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Фактический адрес')]", Keys.ENTER)
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Вид деятельности')]", 'хмм')
        click_and_send_keys(context, ".//INPUT[@data-label='Офиц. номер телефона']", random_phone())
        click_and_send_keys(context, ".//INPUT[@data-label='Должность']", experience + Keys.ENTER)
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Категория должности')]", 'Специалист')
        t.sleep(1)
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Категория должности')]", Keys.ENTER)
        company = 'ПАО СБЕРБАНК'
        # company = "ОТДЕЛЕНИЕ ФИЛИАЛА ООО ЦАБ-ИНДУСТРИТЕХНИК & СЕРВИС ГМБХ (ГЕРМАНИЯ)")
        # company = '9909183340'  # в дадате registration_date = null
        # company = 'ООО "ОЗ ФОРЕНЗИКА"'  # в компании меньше 50 человек и обратилось > 3 человек
        # company = 'ОО "АЗАЗЕЛЬ"'  # компания существует меньше года
        # company = '673107896509'
        # company = 'ООО "ГОСТЬ-2019"'  # компания существует меньше 6 месяцев
        # company = '251118314508'  # ИП существует меньше года
        # company = '450802584949'  # ИП существует больше года
        # company = 'ООО ЧВК "МАР"'  #  спарк - is_being_closed компания находится на стадии ликвидации
        # company = '2308270081'  # компания существует меньше 3 месяцев
        # 'В/Ч 02977'
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Название юр. лица')]", company)
        t.sleep(1)
        click_and_send_keys(context, ".//INPUT[contains(@data-label,'Название юр. лица')]",
                            Keys.DOWN + Keys.RETURN)
        context.browser.find_element_by_xpath(".//INPUT[@data-label='Доход в месяц, руб.']").send_keys(
            f'{money}' + Keys.ENTER)
        t.sleep(1)
        context.browser.find_element_by_tag_name("body").send_keys(Keys.PAGE_DOWN)
        t.sleep(1)
        context.browser.find_element_by_xpath(".//INPUT[@data-label='Фамилия']").send_keys('Цветкова' + Keys.ENTER)
        context.browser.find_element_by_xpath(".//INPUT[@data-label='Имя']").send_keys('Цагана' + Keys.ENTER)
        context.browser.find_element_by_xpath(".//INPUT[@data-label='Отчество']").send_keys('Цыреновна' + Keys.ENTER)
        t.sleep(4)
        context.browser.find_element_by_xpath(".//INPUT[@data-label='Номер телефона']").send_keys(random_phone())
        t.sleep(4)
        context.browser.find_element_by_xpath(".//INPUT[contains(@data-label,'Кем приходится клиенту')]").send_keys(
            'Коллега' + Keys.ENTER)
        t.sleep(11)
        context.browser.find_element_by_class_name('BpButtonNext__wrap').click()

        if 'Частная практика' in job:
            if 'адвокат' in job:
                click_and_send_keys(context, ".//INPUT[@data-label='Реестровый номер']", '1111111' + Keys.ENTER)
                click_and_send_keys(context, ".//INPUT[@data-label='Регион регистрации адвоката']",
                                    'г Москва' + Keys.ENTER)
            else:
                click_and_send_keys(context, ".//INPUT[@data-label='Лицензия']", '1111111' + Keys.ENTER)

        if job in ['Индивидуальный предприниматель', 'Военнослужащий']:
            click_and_send_keys(context, ".//INPUT[@data-label='Категория должности']",
                                'Квалифицированный специалист' + Keys.ENTER)


@Then('Ввод дополнительной информации - "{doc_type}" - "{edu}" - "{children}" - "{martial_status}"')
@make_screenshot_if_fails
def step(context, doc_type, edu, children, martial_status):
    """

    :param context:
    :param doc_type: тип дополнительного документа (Заграничный паспорт/Водительское удостоверение/Военный билет)
    :param edu: тип образования (Неполное среднее/Высшее/)
    :param children: число иждивенцев (для гоcпрограммы >1)
    :return:
    """

    click_and_send_keys(context, ".//INPUT[@data-label='Образование']", f'{edu}' + Keys.ENTER)
    click_and_send_keys(context, ".//INPUT[@data-label='Семейный статус']", f'{martial_status}' + Keys.DOWN + Keys.ENTER)
    # time.sleep(1)
    # click_and_send_keys(context, ".//INPUT[@data-label='Семейный статус']", 'В разводе' + Keys.ENTER)
    click_and_send_keys(context, ".//INPUT[@data-label='Лиц на иждивении']", f'{children}')
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Фамилия']").send_keys('Цветковаа' + Keys.ENTER)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Имя']").send_keys('Цагана' + Keys.ENTER)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Отчество']").send_keys('Цыреновна' + Keys.ENTER)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Дата рождения']").send_keys('16.01.1992' + Keys.ENTER)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Номер телефона']").send_keys(random_phone())
    t.sleep(1)
    click_and_send_keys(context, ".//INPUT[@data-label='Тип доп. документа']", f'{doc_type}' + Keys.ENTER)

    CLIENTS['client']['license-issuedAt'] = '10.10.2018'
    if doc_type == 'Военный билет':
        CLIENTS['client']['license-number'] = 'АВ34567890'
    elif doc_type == 'Водительское удостоверение':
        CLIENTS['client']['license-number'] = '1234123456'
    else:
        CLIENTS['client']['license-number'] = '123411156789'

    click_and_send_keys(context, ".//INPUT[@data-label='Серия и номер']",
                        CLIENTS['client']['license-number'] + Keys.ENTER)
    click_and_send_keys(context, ".//INPUT[@data-label='Дата выдачи']",
                        CLIENTS['client']['license-issuedAt'] + Keys.ENTER)

    context.browser.find_element_by_class_name('BpUpload__input').send_keys(
        os.path.join(cwd, 'pythonProject/features/docs', 'doggo.jpg'))
    time.sleep(3)
    context.browser.find_element_by_class_name('BpButtonNext__wrap').click()


@Then('Открываем калькулятор "{ts_type}" - "{term}" - "{ts}" - "{init_payment}"')
@make_screenshot_if_fails
def step(context, ts_type, term, ts, init_payment):
    """
    :param context:
    :param ts_type: тип ТС (Новый/С пробегом)
    :param term: срок кредита
    :param ts: стоимость ТС
    :param init_payment: первоначальный взнос
    """
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located((By.XPATH, "//*[text()[contains(.,'Подтверждающий документ')]]"))
    )
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Укажите параметры кредита')]]").click()
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located((By.XPATH, "//*[text()[contains(.,'Калькулятор')]]"))
    )
    context.browser.find_element_by_xpath(f"//*[text()[contains(.,'{ts_type}')]]").click()
    time.sleep(1.5)

    context.browser.find_element_by_xpath(".//INPUT[@data-label='Марка']").send_keys('AUDI' + Keys.ENTER)
    time.sleep(1.5)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Модель']").send_keys('A1' + Keys.ENTER)
    time.sleep(1.5)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Стоимость ТС, руб.']").send_keys('\b\b\b\b\b\b\b')
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Стоимость ТС, руб.']").send_keys(ts)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='ПВ, руб.']").send_keys('\b\b\b\b\b\b\b')
    context.browser.find_element_by_xpath(".//INPUT[@data-label='ПВ, руб.']").send_keys(init_payment)
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Срок кредита, мес.']").send_keys('\b\b' + term)
    t.sleep(3)
    context.browser.find_element_by_class_name('BpButtonNext__wrap').click()
    WebDriverWait(context.browser, 120).until(
        EC.invisibility_of_element((By.XPATH, ".//INPUT[@data-label='Калькулятор']"))
    )


@Then('Подтверждение дохода "{doc_type}"')
@make_screenshot_if_fails
def step(context, doc_type):
    """
    :param context:
    :param doc_type: тип документа для подтврерждения дохода (Без подтверждения/Выписка из ПФР/
    Выписка по счёту в другом Банке/Справка по форме Банка/3-НДФЛ/Д)
    :return:
    """
    WebDriverWait(context.browser, 120).until(
        EC.visibility_of_element_located((By.XPATH, ".//INPUT[@data-label='Подтвержд. дохода']"))
    )
    context.browser.find_element_by_xpath(".//INPUT[@data-label='Подтвержд. дохода']").send_keys(
        f'{doc_type}' + Keys.ENTER)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Отправить в банк')]]").click()
    if doc_type != 'Без подтверждения':
        context.browser.find_element_by_class_name('BpUpload__input').send_keys(os.path.join(cwd, 'pythonProject'
                                                                                                  '/features/docs',
                                                                                             'doggo.jpg'))
        time.sleep(4)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Отправить в банк')]]").click()
