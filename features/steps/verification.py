from datetime import datetime

from behave import *
from selenium.webdriver.common.keys import Keys

from common import *
from const import *


@Then('Переходим к верификации документов')
@make_screenshot_if_fails
def step(context):
    context.browser.execute_script("window.open('https://verification.verif-st1.cloud.b-pl.pro/admin','_blank');")
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1.5)
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.ID, "username"))
    )


@Then('Переходим к верификации документов второй раз')
@make_screenshot_if_fails
def step(context):
    context.browser.execute_script("window.open('https://verification.verif-st1.cloud.b-pl.pro/admin','_blank');")
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(240)
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.ID, "query"))
    )


@Then('Проходим процедуру авторизации для "{user}" и "{password}"')
@make_screenshot_if_fails
def step(context, user, password):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.ID, "username"))
    )
    context.browser.find_element_by_id('username').send_keys(user)
    context.browser.find_element_by_id('password').send_keys(password)
    context.browser.find_element_by_id('kc-login').click()


@Then('Очищаем назначенные проверки на "{user}"')
@make_screenshot_if_fails
def step(context, user):
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Проверяет')]]").click()
    time.sleep(1)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Проверяет')]]").click()
    time.sleep(1)

    for el in context.browser.find_element_by_class_name('table-responsive').find_elements_by_xpath(
            f"//*[text()[contains(.,'{user}')]]")[1:]:
        el.find_element_by_xpath("//I[@class='fa fa-times']").click()
        time.sleep(1)
    # context.browser.find_element_by_xpath("//th[@data-property-name='id']").click()
    context.browser.find_element_by_xpath("//th[@class='  integer ']").click()
    # class="  integer "
    time.sleep(1)


@Then('Закрываем вкладку с верификацией')
@make_screenshot_if_fails
def step(context):
    time.sleep(2)
    context.browser.switch_to.window(context.browser.window_handles[-1])
    close(context)
    context.browser.switch_to.window(context.browser.window_handles[0])
    time.sleep(1)


@Then('Верификация Паспорта для "{client}"')
@make_screenshot_if_fails
def step(context, client):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.NAME, "query"))
    )

    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        context.browser.find_element_by_name('query').send_keys(os.environ["id"]+Keys.RETURN)
    time.sleep(2)

    while True:
        try:
            time.sleep(2)
            title = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//*[text()[contains(.,'Паспорт:')]]") if el.get_attribute('title') != '']
            print(f'title = {title}')
            print('')
            if 'кем' in title[0]:
                print('Проверка кем')
                check_issued_by(context, client)
                context.browser.refresh()
            if 'ФИО' in title[0]:
                print('Проверка ФИО')
                check_fio(context, client)
                context.browser.refresh()
            if 'адрес' in title[0]:
                print('Проверка адреса')
                check_address(context, client)
                context.browser.refresh()
        except:
            counter = 0
            while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
                if counter != 5:
                    print('counter = ', counter)
                    time.sleep(1)
                    counter += 1
                    context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
                else:
                    break
            break
    time.sleep(2)


@Then('Верификация для "{client}" Тегирование, Дети, Семейное положение')
@make_screenshot_if_fails
def step(context, client):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.NAME, "query"))
    )

    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        context.browser.find_element_by_name('query').send_keys(os.environ["id"]+Keys.RETURN)
    time.sleep(2)

    while True:
        try:
            time.sleep(2)
            title = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//*[text()[contains(.,'Паспорт:')]]") if el.get_attribute('title') != '']
            print(f'title = {title}')
            print('')
            if 'тегирование' in title[0]:
                print('Проверка тегирование')
                tag(context, client)
                context.browser.refresh()
            if 'дети' in title[0]:
                print('Проверка дети')
                children(context, client)
                context.browser.refresh()
            if 'семейное положение' in title[0]:
                print('Проверка семейное положение')
                sp(context, client)
                context.browser.refresh()
        except:
            counter = 0
            while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
                if counter != 5:
                    print('counter = ', counter)
                    time.sleep(1)
                    counter += 1
                    context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
                else:
                    break
            break
    time.sleep(2)


@Then('Верификация для "{client}" СОПД, Согласие Цафт')
@make_screenshot_if_fails
def step(context, client):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.NAME, "query"))
    )

    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        context.browser.find_element_by_name('query').send_keys(os.environ["id"]+Keys.RETURN)
    time.sleep(2)

    while True:
        try:
            time.sleep(2)
            title2 = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//*[text()[contains(.,'Согласие')]]") if el.get_attribute('title') != '']
            print(f'title = {title2}')
            print('')
            if 'Согласие на обработку персональных данных' in title2[0]:
                print('Проверка СОПД')
                sopd(context, client)
                context.browser.refresh()
            if 'Согласие в ПАО БАНК СИАБ' in title2[0]:
                print('Согласие в ПАО БАНК СИАБ')
                caft(context, client)
                context.browser.refresh()
        except:
            counter = 0
            while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
                if counter != 5:
                    print('counter = ', counter)
                    time.sleep(1)
                    counter += 1
                    context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
                else:
                    break
            break
    time.sleep(2)


@Then('RGS Ждем ВУ для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        order = CLIENTS.get(client).get('order')
        context.browser.find_element_by_name('query').send_keys(f'req:{order}' + Keys.RETURN)
    time.sleep(2)

    set_verification_user(context, locator='ВУ', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )
    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.XPATH, "//*[text()[contains(.,'Отметить как скан с фото')]]"))
    )
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Отметить как скан с фото')]]").click()
    context.browser.find_element_by_id('issuedAt').click()
    context.browser.find_element_by_id('issuedAt').send_keys(CLIENTS.get(client).get('license-issuedAt'))

    context.browser.find_element_by_id('INPUT_DRIVER_LICENSE_SERIES_NUMBER').click()
    context.browser.find_element_by_id('INPUT_DRIVER_LICENSE_SERIES_NUMBER').send_keys(CLIENTS.get(client).get('license-number'))

    time.sleep(2)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)


@Then('RGS Переходим к верификации ПТС для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.browser.refresh()
    time.sleep(2)
    while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
        time.sleep(1)
        context.browser.find_element_by_name('query').send_keys(Keys.RETURN)

    set_verification_user(context, locator='ПТС', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_id('vin').click()
        context.browser.find_element_by_id('vin').send_keys(CLIENTS.get(client).get('pts-vin'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('inputVehiclePassportSeriesNumber').click()
        context.browser.find_element_by_id('inputVehiclePassportSeriesNumber').send_keys(
            CLIENTS.get(client).get('pts-number'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('brand').click()
        context.browser.find_element_by_id('brand').send_keys('Mitsubishi')
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('model').click()
        context.browser.find_element_by_id('model').send_keys('Pajero' + Keys.ENTER)
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('year').click()
        context.browser.find_element_by_id('year').send_keys(CLIENTS.get('fake').get('pts-tsDate'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('engineNumber').click()
        context.browser.find_element_by_id('engineNumber').send_keys(CLIENTS.get('fake').get('pts-model'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('chassis').click()
        context.browser.find_element_by_id('chassis').send_keys(CLIENTS.get('fake').get('pts-shassy'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('color').click()
        context.browser.find_element_by_id('color').send_keys(CLIENTS.get('fake').get('pts-tsColor'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('engineCapacity').click()
        context.browser.find_element_by_id('engineCapacity').send_keys(CLIENTS.get('fake').get('pts-model'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'Бензин')]]").click()
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('issuedAt').click()
        context.browser.find_element_by_id('issuedAt').send_keys(
            CLIENTS.get(client).get('pts-issuedAt'))
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_id('enginePower').click()
        context.browser.find_element_by_id('enginePower').send_keys('1234')
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'л.с.')]]").click()
    except:
        pass
    time.sleep(1)

    try:
        context.browser.find_element_by_class_name('Selector__item').click()
    except:
        pass

    time.sleep(1)
    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(1)
    context.browser.find_element_by_class_name('Button__content').click()
    time.sleep(1)
    close(context)

    context.browser.switch_to.window(context.browser.window_handles[0])


def check_all_switches(context, switch_locator):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, switch_locator))
    )

    for element in context.browser.find_elements_by_class_name(switch_locator):
        element.click()
    time.sleep(2)
    pass


@Then('RGS ДОРАБОТКА Переходим к верификации ПТС для "{client}" - "{user}" - "{field}"')
@make_screenshot_if_fails
def step(context, client, user, field):
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.browser.refresh()
    time.sleep(2)
    while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
        time.sleep(1)
        context.browser.find_element_by_name('query').send_keys(Keys.RETURN)

    set_verification_user(context, user)
    time.sleep(1)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    check_all_switches(context, 'Switch__right')

    try:
        context.browser.find_element_by_id('vin').click()
        if field == 'vin' or field == 'VIN':
            context.browser.find_element_by_id('vin').send_keys('00000000000000000')
        else:
            context.browser.find_element_by_id('vin').send_keys(CLIENTS.get(client).get('pts-vin'))
    except:
        pass

    try:
        context.browser.find_element_by_id('inputVehiclePassportSeriesNumber').click()
        context.browser.find_element_by_id('inputVehiclePassportSeriesNumber').send_keys(
            CLIENTS.get(client).get('pts-number'))
    except:
        pass

    try:
        context.browser.find_element_by_id('brand').click()
        context.browser.find_element_by_id('brand').send_keys('BAW')
    except:
        pass

    try:
        context.browser.find_element_by_id('model').click()
        context.browser.find_element_by_id('model').send_keys('Fenix 1065' + Keys.ENTER)
    except:
        pass

    try:
        context.browser.find_element_by_id('year').click()
        context.browser.find_element_by_id('year').send_keys(CLIENTS.get('fake').get('pts-tsDate'))
    except:
        pass

    try:
        context.browser.find_element_by_id('engineNumber').click()
        context.browser.find_element_by_id('engineNumber').send_keys(CLIENTS.get('fake').get('pts-model'))
    except:
        pass

    try:
        context.browser.find_element_by_id('chassis').click()
        context.browser.find_element_by_id('chassis').send_keys(CLIENTS.get('fake').get('pts-shassy'))
    except:
        pass

    try:
        context.browser.find_element_by_id('color').click()
        if field == 'color':
            context.browser.find_element_by_id('color').send_keys('1')
        else:
            context.browser.find_element_by_id('color').send_keys(CLIENTS.get('fake').get('pts-tsColor'))
    except:
        pass

    try:
        context.browser.find_element_by_id('engineCapacity').click()
        context.browser.find_element_by_id('engineCapacity').send_keys(CLIENTS.get('fake').get('pts-model'))
    except:
        pass

    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'Бензин')]]").click()
    except:
        pass

    try:
        context.browser.find_element_by_id('issuedAt').click()
        context.browser.find_element_by_id('issuedAt').send_keys(
            CLIENTS.get(client).get('pts-issuedAt'))
    except:
        pass

    try:
        context.browser.find_element_by_id('enginePower').click()
        context.browser.find_element_by_id('enginePower').send_keys('1234')
    except:
        pass

    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'л.с.')]]").click()
    except:
        pass

    try:
        context.browser.find_element_by_class_name('Selector__item').click()
    except:
        pass

    time.sleep(2)
    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(5)
    context.browser.find_element_by_class_name('Button__content').click()
    time.sleep(5)
    close(context)


def set_verification_user(context, locator=None, user=None):
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.XPATH, "//*[text()[contains(.,'Назначить')]]"))
    )
    if locator:
        btn = context.browser.find_element_by_xpath(f'//a[contains(@title,"{locator}")]/ancestor::tr//a[@data-job-id]')
    else:
        btn = context.browser.find_element_by_xpath("//*[text()[contains(.,'Назначить')]]")
    btn.click()
    time.sleep(2)
    context.browser.find_element_by_xpath('//span[contains(@class,"select2-search")]//input').send_keys(
        'Котов' + Keys.RETURN)
    time.sleep(1)


def check_issued_by(context, client):
    set_verification_user(context, locator='Паспорт: кем')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_id('issuedBy').click()
        context.browser.find_element_by_id('issuedBy').send_keys(CLIENTS.get(client).get('issuedBy'))
    except:
        pass

    try:
        context.browser.find_element_by_id('issuedAt').click()
        context.browser.find_element_by_id('issuedAt').send_keys(CLIENTS.get(client).get('issuedAt'))
    except:
        pass

    try:
        context.browser.find_element_by_id('divisionCode').click()
        context.browser.find_element_by_id('divisionCode').send_keys(CLIENTS.get(client).get('divisionCode'))
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


def tag(context, client):
    set_verification_user(context, locator='тегирование')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').click()
        context.browser.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(CLIENTS.get(client).get('passport'))
    except:
        pass

    try:
        context.browser.find_element_by_id('firstSpread').click()
        context.browser.find_element_by_id('signature').send_keys(Keys.ARROW_RIGHT)
    except:
        pass

    try:
        context.browser.find_element_by_id('signature').click()
        context.browser.find_element_by_id('registrationFirst').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('registrationFirst').click()
        context.browser.find_element_by_id('registrationSecond').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('registrationSecond').click()
        context.browser.find_element_by_id('registrationThird').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('registrationThird').click()
        context.browser.find_element_by_id('registrationFourth').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('registrationFourth').click()
        context.browser.find_element_by_id('militaryDuty').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('militaryDuty').click()
        context.browser.find_element_by_id('maritalStatus').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('maritalStatus').click()
        context.browser.find_element_by_id('children').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('children').click()
        context.browser.find_element_by_id('previouslyIssued').send_keys(Keys.ARROW_RIGHT)
    except:
        pass
    try:
        context.browser.find_element_by_id('previouslyIssued').click()
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


def children(context, client):
    set_verification_user(context, locator='дети')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_xpath("(//button[@class='Selector__item'])[1]").click()
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


def sp(context, client):
    set_verification_user(context, locator='семейное положение')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        for element in context.browser.find_elements_by_xpath("(//button[@class='Selector__item'][2])"):
            element.click()
        time.sleep(2)
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


def sopd(context, client):
    set_verification_user(context, locator='Согласие на обработку персональных данных')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)
    try:
        context.browser.find_element_by_id('documentTypeCode').click()
        time.sleep(1)
        context.browser.find_element_by_id('documentTypeCode').send_keys(Keys.ARROW_DOWN + Keys.ENTER)
    except:
        pass
    WebDriverWait(context.browser, 120).until(EC.element_to_be_clickable(
        (By.CLASS_NAME, 'Switch__right')))

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()

    time.sleep(2)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)


def caft(context, client):
    set_verification_user(context, locator='Согласие в ПАО БАНК СИАБ')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


@Then('Проходим верификацию ВУ для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    while True:
        try:
            time.sleep(2)
            title = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//*[text()[contains(.,'ВУ')]]") if el.get_attribute('title') != '']
            if title:
                break
            context.browser.refresh()
        except:
            pass

    set_verification_user(context, locator='ВУ', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_id('doc-date-of-birth').click()
        context.browser.find_element_by_id('doc-date-of-birth').send_keys(CLIENTS.get(client).get('birthday'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-driving-license-number').click()
        context.browser.find_element_by_id('doc-driving-license-number').send_keys(CLIENTS.get(client).get(
            'license-number'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-date-of-issue').click()
        context.browser.find_element_by_id('doc-date-of-issue').send_keys(CLIENTS.get(client).get('license-issuedAt'))
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    close(context)
    time.sleep(3)


def check_fio(context, client):
    set_verification_user(context, locator='Паспорт: ФИО')
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)

    try:
        context.browser.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').click()
        context.browser.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(
            CLIENTS.get(client).get('passport'))
    except:
        pass

    try:
        context.browser.find_element_by_id('lastName').send_keys(CLIENTS.get(client).get('lastName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('secondName').send_keys(CLIENTS.get(client).get('secondName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('firstName').send_keys(CLIENTS.get(client).get('firstName'))
    except:
        pass

    try:
        if CLIENTS.get(client).get('sex') == 'Жен':
            context.browser.find_elements_by_class_name('Selector__item')[1].click()
        else:
            context.browser.find_elements_by_class_name('Selector__item')[0].click()
    except:
        pass

    try:
        context.browser.find_element_by_id('birthday').click()
        context.browser.find_element_by_id('birthday').send_keys(CLIENTS.get(client).get('birthday'))
    except:
        pass

    try:
        context.browser.find_element_by_id('birthPlace').send_keys(CLIENTS.get(client).get('birthPlace'))
    except:
        pass
    time.sleep(2)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)


def check_address(context, client):
    set_verification_user(context, locator='Паспорт: адрес')
    context.browser.switch_to.window(context.browser.window_handles[-1])
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()

    context.browser.find_element_by_id('registrationAddress').send_keys(CLIENTS.get(client).get('reg'))
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.XPATH, "//DIV[@class='suggestions__item']"))
        )
    context.browser.find_element_by_xpath("//DIV[@class='suggestions__item']").click()
    time.sleep(3)
    context.browser.find_element_by_xpath("//DIV[@class='Button__content']").click()
    close(context)
    time.sleep(2)


def close(context):
    context.browser.close()
    time.sleep(0.5)
    context.browser.switch_to.window(context.browser.window_handles[-1])


@Then('RGS Переходим к верификации счетов для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.browser.refresh()
    time.sleep(2)
    try:
        while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
    except:
        time.sleep(5)

    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        order = CLIENTS.get(client).get('order')
        context.browser.find_element_by_name('query').send_keys(f'req:{order}'+Keys.RETURN)
    time.sleep(5)

    while True:
        try:
            time.sleep(1.5)
            title = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//*[text()[contains(.,'Счет')]]") if el.get_attribute('title') != '']
            print('title = ', title)
            print('')
            if len(title) == 0:
                break
            if 'ТС' in title[0]:
                print('Проверка счета на оплату ТС')
                check_schet(context, client, user=user)
                context.browser.refresh()
            for ins in CLIENTS.get(client).get('account').get('ins'):
                if insurance_map[ins]['invoice'] in title[0]:
                    print('Проверка ', ins)
                    check_insses(context, client, ins, user=user)
                    context.browser.refresh()
        except:
            break
    print('')
    time.sleep(2)


def check_schet(context, client, user):
    set_verification_user(context, locator='Счет на оплату ТС', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )
    try:
        for element in context.browser.find_elements_by_class_name('Switch__right'):
            element.click()
        time.sleep(2)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-invoice-number').send_keys(
            CLIENTS.get(client).get('invoice-number') + Keys.ENTER)
        context.browser.find_element_by_id('doc-invoice-number').send_keys('1' + Keys.ENTER)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-billing-date').click()
        context.browser.find_element_by_id('doc-billing-date').send_keys(datetime.today().strftime('%d.%m.%Y'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-inn').click()
        context.browser.find_element_by_id('doc-inn').send_keys(CLIENTS.get(client).get('account').get('ts').get('inn')
                                                                + Keys.ENTER)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-account-number').click()
        context.browser.find_element_by_id('doc-account-number').send_keys(
            CLIENTS.get(client).get('account').get('ts').get('acc') + Keys.RETURN)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-amount').click()
        context.browser.find_element_by_id('doc-amount').send_keys(CLIENTS.get(client).get('account').get('ts').get('limit'))
    except:
        pass

    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'Нет')]]").click()
    except:
        pass

    time.sleep(2)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)


@Then('RGS Переходим к верификации ДКП для "{client}" - "{user}"')
def check_dkp(context, client, user):
    set_verification_user(context, locator='ДКП', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )
    try:
        for element in context.browser.find_elements_by_class_name('Switch__right'):
            element.click()
        time.sleep(2)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-last-name').send_keys(CLIENTS.get(client).get('lastName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-second-name').send_keys(CLIENTS.get(client).get('secondName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-first-name').send_keys(CLIENTS.get(client).get('firstName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-passport-series-number').click()
        context.browser.find_element_by_id('doc-passport-series-number').send_keys(CLIENTS.get(client).get('passport'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-inn-seller').click()
        context.browser.find_element_by_id('doc-inn-seller').send_keys(
            CLIENTS.get(client).get('account').get('ts').get('inn'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-vin').click()
        context.browser.find_element_by_id('doc-vin').send_keys(CLIENTS.get(client).get('pts-vin'))
    except:
        pass

    try:
        context.browser.find_element_by_id(
            'doc-vehicle-price').send_keys(CLIENTS.get(client).get('account').get('ts').get('price') + Keys.RETURN)
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)
    context.browser.refresh()
    time.sleep(2)


@Then('RGS ДОРАБОТКА Переходим к верификации ДКП для "{client}" - "{user}"')
def check_dkp_fail(context, client, user):
    set_verification_user(context, locator='ДКП', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__left'))
    )
    try:
        for element in context.browser.find_elements_by_class_name('Switch__left'):
            element.click()
        time.sleep(2)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-last-name').send_keys(CLIENTS.get(client).get('lastName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-second-name').send_keys(CLIENTS.get(client).get('secondName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-first-name').send_keys(CLIENTS.get(client).get('firstName'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-passport-series-number').click()
        context.browser.find_element_by_id('doc-passport-series-number').send_keys(CLIENTS.get(client).get('passport'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-inn-seller').click()
        context.browser.find_element_by_id('doc-inn-seller').send_keys(
            CLIENTS.get(client).get('account').get('ts').get('inn'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-vin').click()
        context.browser.find_element_by_id('doc-vin').send_keys(CLIENTS.get(client).get('pts-vin'))
    except:
        pass

    try:
        context.browser.find_element_by_id(
            'doc-vehicle-price').send_keys(CLIENTS.get(client).get('account').get('ts').get('price') + Keys.RETURN)
    except:
        pass
    time.sleep(1.5)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)
    context.browser.refresh()
    time.sleep(2)


@Then('RGS Переходим к верификации фото для "{client}" - "{user}"')
def check_photo(context, client, user):
    set_verification_user(context, locator='Сверка фото', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'OldSwitch__right'))
    )
    try:
        for element in context.browser.find_elements_by_class_name('OldSwitch__right'):
            element.click()
        time.sleep(2)
    except:
        pass

    time.sleep(2)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)


def check_insses(context, client, type, user):
    set_verification_user(context, locator='(', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )
    try:
        for element in context.browser.find_elements_by_class_name('Switch__right'):
            element.click()
        time.sleep(2)
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-inn').click()
        context.browser.find_element_by_id('doc-inn').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('inn'))
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-bik').click()
        context.browser.find_element_by_id('doc-bik').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('bik'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-account-number').click()
        context.browser.find_element_by_id('doc-account-number').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('acc'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-invoice-number').click()
        context.browser.find_element_by_id('doc-invoice-number').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('invoice-number'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-amount').click()
        context.browser.find_element_by_id('doc-amount').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('amount'))
    except:
        pass

    try:
        context.browser.find_element_by_id('doc-billing-date').click()
        context.browser.find_element_by_id('doc-billing-date').send_keys(
            CLIENTS.get(client).get('account').get('ins').get(type).get('date'))
    except:
        pass

    try:
        context.browser.find_elements_by_class_name('RadioButton')[-1].click()
    except:
        pass
    time.sleep(5)
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)


@Then('RGS Переходим к верификации подписей для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.browser.refresh()
    time.sleep(10)

    try:
        while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
    except:
        pass

    if context.browser.find_element_by_name('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        order = CLIENTS.get(client).get('order')
        context.browser.find_element_by_name('query').send_keys(f'req:{order}'+Keys.RETURN)
    time.sleep(2)

    sign_checks = ['Анкета', 'Индивидуальные условия', 'Договор залога', 'Помощь на дороге',
                   'Полис КАСКО', 'Полис ВместоКАСКО', 'Полис GAP', 'Полис страхование жизни']
    while True:
        try:
            time.sleep(1.5)
            # title = [el.text for el in context.browser.find_elements_by_xpath(
            #     "//td[@data-label='Тип']") if el.text != '']
            title = [el.get_attribute('title') for el in context.browser.find_elements_by_xpath(
                "//nobr/*") if el.get_attribute('title') != '']
            # title = [el.text for el in context.browser.find_elements_by_xpath(
            #     "//td[@data-label='Тип']") if el.text != '']
            print('Текущие проверки = ', title)
            if len(title) == 0:
                break
            try:
                doc_type = [el for el in sign_checks if el in title[0]][-1]
            except:
                doc_type = None

            if 'Проверка подписей' in title[0] and doc_type \
                    or 'Соглашение о взаимодействии' in title[0]:
                print(f'Проверка подписей {title[0]}')
                check_sign(context, user=user, doc_type=doc_type if doc_type else None,
                           locator='Проверка подписей' if 'Проверка подписей'in title[0] and doc_type
                           else 'Соглашение о взаимодействии')
                context.browser.refresh()
            elif 'Квитанция' in title[0]:
                print(f'Проверка {title[0]}')
                check_statement(context, client, user=user)
                context.browser.refresh()
            elif 'Полис' in title[0]:
                polis_type = title[0].split('Полис ')[-1]
                polis_type = 'Страхование жизни' if polis_type == 'СЖ' else polis_type
                polis_type = 'ДКАСКО (GAP)' if polis_type == 'страхования ТС' else polis_type
                print(f'Проверка {title[0]}')
                check_polis(context, client, user=user, polis_type=polis_type)
                context.browser.refresh()
        except:
            break
    print('')
    time.sleep(2)


def check_statement(context, client, user):
    set_verification_user(context, locator='Квитанция', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(EC.element_to_be_clickable(
        (By.CLASS_NAME, 'Switch__right')))
    for el in context.browser.find_elements_by_class_name('Switch__right'):
        el.click()

    try:
        print(CLIENTS.get(client).get('account').get('ins').get('КАСКО').get('amount'))
        print(type(CLIENTS.get(client).get('account').get('ins').get('КАСКО').get('amount')))
        context.browser.find_element_by_id('doc-amount').click()
        context.browser.find_element_by_id('doc-amount').send_keys(
            CLIENTS.get(client).get('account').get('ins').get('КАСКО').get('amount'))
    except Exception as e:
        context.browser.find_element_by_id('doc-amount').send_keys('20000')
        print(e)
        print('')
    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)
    context.browser.refresh()


def check_polis(context, client, user, polis_type):
    set_verification_user(context, locator='Полис', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])

    WebDriverWait(context.browser, 120).until(EC.element_to_be_clickable(
        (By.CLASS_NAME, 'Switch__right')))
    for el in context.browser.find_elements_by_class_name('Switch__right'):
        el.click()

    try:
        context.browser.find_element_by_id('doc-series-number-of-policy').click()
        context.browser.find_element_by_id('doc-series-number-of-policy').send_keys(
            CLIENTS[client]['account']['ins'][polis_type]['number'])
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-last-name').click()
        context.browser.find_element_by_id('doc-last-name').send_keys(CLIENTS[client]['lastName'])
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-first-name').click()
        context.browser.find_element_by_id('doc-first-name').send_keys(CLIENTS[client]['firstName'])
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-vin-number').click()
        context.browser.find_element_by_id('doc-vin-number').send_keys(CLIENTS[client]['pts-vin'])
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-contract-term').click()
        context.browser.find_element_by_id('doc-contract-term').send_keys(CLIENTS[client]['account']['ins'][polis_type]['date'])
    except Exception as e:
        pass

    try:
        context.browser.find_element_by_id('doc-insurance-premium').click()
        context.browser.find_element_by_id('doc-insurance-premium').send_keys(CLIENTS[client]['account']['ins'][polis_type]['amount'])
    except Exception as e:
        pass

    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)
    context.browser.refresh()


def check_sign(context, user, doc_type=None, locator=None):
    set_verification_user(context, locator=locator, user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])
    try:
        context.browser.find_element_by_xpath("//*[text()[contains(.,'Тип документа')]]").click()
        time.sleep(1)
        context.browser.find_element_by_xpath(f"//*[text()[contains(.,'{doc_type}')]]").click()
    except:
        pass
    WebDriverWait(context.browser, 120).until(EC.element_to_be_clickable(
        (By.CLASS_NAME, 'Switch__right')))

    try:
        num = context.browser.find_element_by_class_name('block__heading').text.split(' ')[-1]
    except:
        num = 0
    for i in range(int(num if num else 1)):
        for el in context.browser.find_elements_by_class_name('Switch__right'):
            el.click()
        time.sleep(2)

    context.browser.find_element_by_class_name('Button__content').click()
    close(context)
    time.sleep(2)
    context.browser.refresh()


@Then('RGS Переходим к верификации звонков для "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    CLIENTS[client]['order'] = '30004725'
    context.browser.execute_script(
        "window.open('https://underwriting-app-1.auto-rgsb-st1.cloud.b-pl.pro/admin/','_blank');")
    time.sleep(2.5)

    context.browser.switch_to.window(context.browser.window_handles[-1])
    time.sleep(2)
    # WebDriverWait(context.browser, 20).until(
    #     EC.element_to_be_clickable(
    #         (By.NAME, "login"))
    # )
    #
    # context.browser.find_element_by_id('username').send_keys('e.rybakova@balance-pl.ru')
    # context.browser.find_element_by_id('password').send_keys('e.rybakova@balance-pl.ru' + Keys.RETURN)
    # time.sleep(5)
    #
    # pass


@Then('RGS Находим заявку в андеррайтинге "{client}" - "{user}"')
@make_screenshot_if_fails
def step(context, client, user):
    context.browser.refresh()
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.ID, "query"))
    )

    if context.browser.find_element_by_id('query').get_attribute('value') != '':
        context.browser.refresh()
    else:
        time.sleep(2)
        order = CLIENTS.get(client).get('order')
        context.browser.find_element_by_id('query').send_keys(f'{order}' + Keys.RETURN)
    time.sleep(2)

    while True:
        try:
            time.sleep(1.5)
            title = [el.get_attribute('title')
                     for el in context.browser.find_elements_by_xpath("//*[text()[contains(.,'Звонок')]]")
                     if el.get_attribute('title') != '']
            if 'занятости и доходов' in title[0]:
                print('Проверка кем')
                call_work(context, client, user=user)
                context.browser.refresh()
        except:
            while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
                time.sleep(1)
                context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
            break
    time.sleep(2)
    print('')
    pass


def call_work(context, client, user):
    set_verification_user(context, user)
    context.browser.switch_to.window(context.browser.window_handles[2])
    time.sleep(1)

    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, "number-item"))
    )
    context.browser.find_element_by_class_name('number-item').click()
    time.sleep(1)
    context.browser.find_elements_by_class_name('question-type-tab')[1].click()
    time.sleep(3)
    el = context.browser.find_element_by_xpath("//label[@title='представились изначально/ автопредставление']")
    print('found')
    a = el.find_elements_by_xpath('//input[contains(@id, "представились изначально/ автопредставление")]')
    print(len(a))
    try:
        a[1].click()
    except Exception as e:
        print(e)
    print('all')

    time.sleep(5)
    print('')


@Then('RGS Верификация свитедельства о рождении для "{client}" - "{user}"')
def step(context, client, user):
    time.sleep(0.5)
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.browser.refresh()
    time.sleep(2)
    while context.browser.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
        time.sleep(1)
        context.browser.find_element_by_name('query').send_keys(Keys.RETURN)
    set_verification_user(context, locator='Свидетельство о рождении/усыновлении', user=user)
    context.browser.switch_to.window(context.browser.window_handles[-1])
    WebDriverWait(context.browser, 120).until(
        EC.element_to_be_clickable(
            (By.CLASS_NAME, 'Switch__right'))
    )

    for element in context.browser.find_elements_by_class_name('Switch__right'):
        element.click()
    time.sleep(2)
    try:
        context.browser.find_element_by_id('doc-parent-last-name-1').send_keys(CLIENTS.get(client).get('lastName'))
        context.browser.find_element_by_id('doc-parent-first-name-1').send_keys(CLIENTS.get(client).get('firstName'))
        context.browser.find_element_by_id('doc-parent-patronymic-name-1').send_keys(CLIENTS.get(client).get('secondName'))
    except:
        pass
    context.browser.find_element_by_id('doc-date-of-birth-1').click()
    time.sleep(0.5)
    context.browser.find_element_by_id('doc-date-of-birth-1').send_keys('01012020')
    time.sleep(0.5)
    try:
        context.browser.find_element_by_id('doc-parent-last-name-2').send_keys(CLIENTS.get(client).get('lastName'))
        context.browser.find_element_by_id('doc-parent-first-name-2').send_keys(CLIENTS.get(client).get('firstName'))
        context.browser.find_element_by_id('doc-parent-patronymic-name-2').send_keys(CLIENTS.get(client).get('secondName'))
    except:
        pass

    time.sleep(0.5)
    context.browser.find_element_by_id('doc-date-of-birth-2').click()
    context.browser.find_element_by_id('doc-date-of-birth-2').send_keys('01012020')
    time.sleep(1.5)
    context.browser.find_element_by_xpath("//*[text()[contains(.,'Готово')]]").click()
    time.sleep(1.5)
    close(context)
    context.browser.switch_to.window(context.browser.window_handles[0])

# @Then('RGS Ждем скоринг для "{client}"')
# def step(context, client):
#     cur_state = ''
#     while cur_state != 'scoring_state':
#         sql_client = "SELECT sm.current_state " \
#                      "FROM walkers w JOIN state_machines as sm ON w.id=sm.walker_id " \
#                      f"WHERE w.bpm_id = '{CLIENTS.get(client).get('order')}' "
#         conn = psycopg2.connect(**psql_conn_params)
#         cur = conn.cursor()
#         cur.execute(sql_client)
#         cur_state = cur.fetchall()[-1][0]
#         time.sleep(5)
#
#     sql_client = "SELECT raw -> 'params' -> 'scoring_num' " \
#                  "FROM events e JOIN walkers w on e.walker_id = w.id " \
#                  f"WHERE w.bpm_id = '{CLIENTS.get(client).get('order')}' AND " \
#                  "resource = 'scoring' AND action = 'create'"
#     conn = psycopg2.connect(**psql_conn_params)
#     cur = conn.cursor()
#     cur.execute(sql_client)
#     scoring_number = cur.fetchall()[0][0]
#
#     with open('scoring_data.txt', 'r') as f:
#         config_data = load(f)
#         config_data['request_id'] = str(CLIENTS.get(client).get('order'))
#         stage = 'amqp://bpm-auto-rgsb-st1:7945ff878eb@10.112.6.13:5672/auto-rgsb-st1'
#
#         connection = pika.BlockingConnection(pika.URLParameters(stage))
#         channel = connection.channel()
#
#         channel.basic_publish(exchange='services',
#                               # routing_key='st1.scoring',
#                               routing_key='faker',
#                               body=dumps(config_data, ensure_ascii=False))
#
#         connection.close()
#     a = 8