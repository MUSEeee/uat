import json
import os
from fuzzywuzzy.string_processing import string
from mimesis import Person
from numpy.random import random
import random
import requests
import csv
from dadata import Dadata

# with open('C:/Users/kotov/Desktop/Книга1.csv', encoding='utf-8', newline='') as csvfile:
#     reader = csv.DictReader(csvfile)
#     for row in reader:
#         token = "9a05a9d900a7b2d3ce9f5d5ccacd4f4799480165"
#         secret = "6e6f16747f63505cdab76149ca8ef476f3837736"
#         dadata = Dadata(token, secret)
#         result = dadata.clean("address", row['poselki'])
#         r = json.dumps(result['city'])
#         g = json.loads(r)
#         print(g)
path = '/project'
cwd = os.path.dirname(os.getcwd())
print(cwd)

person = Person('ru')


def random_string():
    letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    return ''.join(random.choice(letters) for i in range(10))


def random_phone():
    digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    return '+7999' + ''.join(random.choice(digits) for i in range(7))


def random_passport():
    digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    return '111106' + ''.join(random.choice(digits) for i in range(4))


FIO = random_string()
passport = random_passport()
CLIENTS = {
    'client': {
        'firstName': FIO,
        'lastName': FIO,
        'secondName': FIO,
        'passport': passport,
        'issuedBy': 'ТП №8 ОТДЕЛА УФМС РОССИИ ПО САНКТ-ПЕТЕРБУРГУ И ЛЕНИНГРАДСКОЙ ОБЛ. В ВАСИЛЕОСТРОВСКОМ Р-НЕ Г. САНКТ-ПЕТЕРБУРГА',
        'reg': 'Московская обл, г Клин, деревня Мякинино, д 1А',
        'sex': '',
        'birthday': '24.03.1992',
        'birthPlace': 'ЛЕНИНГРАД',
        'divisionCode': '780-008',
        'issuedAt': '07.10.2016'
    },
    'fake': {
        'firstName': '',
        'lastName': '',
        'secondName': '',
        'passport': '',
        'issuedBy': 'ТП №8 ОТДЕЛА УФМС РОССИИ ПО САНКТ-ПЕТЕРБУРГУ И ЛЕНИНГРАДСКОЙ ОБЛ. Г. САНКТ-ПЕТЕРБУРГА',
        'reg': 'Московская обл, г Волоколамск, ул Широкая, д 5, кв 2',
        'birthday': '',
        'birthPlace': 'Ленинград',
        'divisionCode': '',
        'issuedAt': '',
        'order': '',
        'license-issuedAt': '',
        'license-number': '',
    },
    'bankrot': {
        'firstName': 'Китайка',
        'lastName': 'Ольга',
        'secondName': 'Олеговна',
        'passport': '4016604033',
        'issuedBy': 'ТП №8 ОТДЕЛА УФМС РОССИИ ПО САНКТ-ПЕТЕРБУРГУ И ЛЕНИНГРАДСКОЙ ОБЛ. В ВАСИЛЕОСТРОВСКОМ Р-НЕ Г. САНКТ-ПЕТЕРБУРГА',
        'reg': "г Санкт-Петербург, ул Камышовая, д 54 к 3, кв 50",
        'birthday': '26.03.1980',
        'birthPlace': 'гор. Ленинград',
        'divisionCode': '780-008',
        'issuedAt': '07.10.2016'
    },
    'inn': {
        'firstName': 'Николай',
        'lastName': 'Фирсов',
        'secondName': 'Борисович',
        'passport': '9803760704',
        'issuedBy': 'Эльдиканским отделением милиции МВД Республики Саха (Якутия)',
        'reg': 'Московская обл, г Волоколамск, ул Широкая, д 5, кв 2',
        'birthday': '03.05.1979',
        'birthPlace': 'пос. Эльдикан Усть-Майского р-на Якутской АССР',
        'divisionCode': '143-019',
        'issuedAt': '03.03.2005'
    },
    'terror': {
        'firstName': 'Фатимат',
        'lastName': 'Султанова',
        'secondName': 'Мирзоевна',
        'passport': '1203915589',
        'issuedBy': 'ОТДЕЛОМ УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛАСТИ В ЦЕНТРАЛЬНОМ РАЙОНЕ Г.ЧЕЛЯБИСНКА',
        'reg': 'РОССИЙСКАЯ ФЕДЕРАЦИЯ, ЧЕЛЯБИНСКАЯ ОБЛАСТЬ, Г. ЧЕЛЯБИНСК, ПР-КТ СВЕРДЛОВСКИЙ, Д. 58, КВ. 4',
        'birthday': '02.02.1984',
        'birthPlace': 'ПОС. АКСАРАЙСКИЙ Г. АСТРАХАНЬ',
        'divisionCode': '780-008',
        'issuedAt': '17.09.2004'
    },
    'igor': {
        'firstName': 'Игорь',
        'lastName': 'Цаган',
        'secondName': 'Владимирович',
        'passport': '4518846250',
        'issuedBy': 'ГУ МВД РОССИИ ПО Г. МОСКВЕ',
        'reg': 'г Москва, ул Пулковская, д 3 к 1, кв 209',
        'birthday': '26.06.1973',
        'birthPlace': 'ГОРОД ПЕРМЬ',
        'divisionCode': '770-017',
        'issuedAt': '11.09.2018',
        'order': '',
        'license-issuedAt': '',
        'license-number': '',
    },
    'robot': {
        'firstName': 'Робот',
        'lastName': 'Мистер',
        'secondName': 'Аниме',
        'passport': '4518846250',
        'issuedBy': 'ГУ МВД РОССИИ ПО Г. МОСКВЕ',
        'reg': 'г Москва, ул Пулковская, д 3 к 1, кв 209',
        'birthday': '26.06.1973',
        'birthPlace': 'ГОРОД ПЕРМЬ',
        'divisionCode': '770-017',
        'issuedAt': '11.09.2018',
        'order': '',
        'license-issuedAt': '',
        'license-number': '',
    }
}


def regenerate_passport():
    CLIENTS['client']['passport'] = person.identifier(mask='1111-######')


PROGRAMS = {
    "Новое авто": "[for=new]",
    "БУ авто": "[for=used]",
    "ПВ0": "[for=pv0]",
    "Рольф": "[for=official_rolf]",
    "Байбэк": "[for=buyback]",
    "БУ официал": "[for=official]",
    "Новое авто Зенит": "[for=zenit_new]",
    "БУ Зенит": "[for=zenit_used]",
    "ОП Зенит": "[for=zenit_buyback]",
    "БУ ПВ0": "[for=used_pv0]",
    "БУ ПВ0 официал": "[for=used_pv0_official]",
    "Первый": "[for=state_first]",
    "Семейный": "[for=state_family]",
    "Официал": "[for=official]",
    "Остаточный": "[for=buyback]",
    "Гос Финанс первый": "[for=new_hyundai_finance_state_first]",

}

ADDRESS = {
    "Адрес": "респ Башкортостан, Буздякский р-н, село Буздяк, ул М.Горького, д 21"
}

PTS = {
    "VIN": "XWEJ3813BK0003554",
    "VIN 2": "X9L00000000000000",
    "VIN 32": "Z9400000000000000",
    "VIN 3": "2c4pc1ggxcr153056",
    "Серия и номер ПТС": "63ОР856245",
    "Серия и номер ПТС2": "78ОТ845900",
    "VIN Z": "X7000000000000000",
}

TEMP = {
    "84": "//TD[@class='FormRequestCreditConditions'][text()='84 мес.']",
    "60": "//TD[@class='FormRequestCreditConditions'][text()='60 мес.']",
    "72": "//TD[@class='FormRequestCreditConditions'][text()='72 мес.']",
    "24": "//TD[@class='FormRequestCreditConditions'][text()='24 мес.']",
    "54": "//TD[@class='FormRequestCreditConditions'][text()='54 мес.']"
}
FILE1 = cwd + '/pythonProject/features/docs/1pass_Ch.jpg'
FILE2 = cwd + '/pythonProject/features/docs/2pass_Ch.jpg'
FILE3 = cwd + '/pythonProject/features/docs/3pass_Ch.jpg'
FILE4 = cwd + '/pythonProject/features/docs/4pass_Ch.jpg'
FILE5 = cwd + '/pythonProject/features/docs/5pass_Ch.jpg'
FILE6 = cwd + '/pythonProject/features/docs/6pass_Ch.jpg'
FILE7 = cwd + '/pythonProject/features/docs/7pass_Ch.jpg'
FILE8 = cwd + '/pythonProject/features/docs/8pass_Ch.jpg'
FILE9 = cwd + '/pythonProject/features/docs/9pass_Ch.jpg'
allF = FILE1 + " \n " + FILE2 + " \n " + FILE3 + " \n " + FILE4 + " \n " + FILE5 + " \n " + FILE6 + " \n " + \
       FILE7 + " \n " + FILE8 + " \n " + FILE9

DOCS = {
}

UPLOAD = {
    "ИУ": "//A[@class='PageRequestStep09__printLink'][text()='Индивидуальные условия']",
    "Анкета": "//A[@class='PageRequestStep09__printLink'][text()='Анкета-заявка']",
    "График": "//A[@class='PageRequestStep09__printLink'][text()='График платежей']",
    "Открытие счетов": "//A[@class='PageRequestStep09__printLink'][text()='Заявл. на открытие счетов']",
    "Перевод за ТС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод за ТС']",
    "Перевод за КАСКО": "(//INPUT[@class='BpUpload__input'])[6]",
    "ФинКаско Зенит": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег ФинКаско Зенит']",
    "Перевод за GAP": "(//INPUT[@class='BpUpload__input'])[7]",
    "Перевод за Дорожную карту": "(//INPUT[@class='BpUpload__input'])[8]",
    "Перевод за Страхование жизни": "(//INPUT[@class='FBppload__input'])[9]",
    "Перевод за Гарантию": "(//INPUT[@class='BpUpload__input'])[10]",
    "Перевод за Ренессанс СЖ": "(//INPUT[@class='BpUpload__input'])[11]",
    "Полис страховки": "(//INPUT[@class='BpUpload__input'])[12]",
    "Гарантийное письмо": "//A[@class='PageRequestStep09__printLink'][text()='Гарантийное письмо']",
    "Договор залога": "//A[@class='PageRequestStep09__printLink'][text()='Договор залога']",
    "Доп соглашение": "//A[@class='PageRequestStep09__printLink'][text()='Дополнительное соглашение']",
    "Приложение 2 к полису GAP Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Приложение 2 к полису GAP Зенит РГС']",
    "Приложение №1 КСЖ (График платежей) РГС": "//A[@class='PageRequestStep09__printLink'][text()='Приложение №1 КСЖ 1 (График платежей) РГС']",
    "Памятка КСЖ РГС": "//A[@class='PageRequestStep09__printLink'][text()='Памятка КСЖ РГС']",
    "Заявление на страхование GAP Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на страхование GAP 1 Зенит РГС']",
    "Заявление на перевод денег КАСКО Партнер": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег КАСКО Партнер']",
    "Заявление на перевод денег GAP Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег GAP Зенит РГС']",
    "Полис GAP Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Полис GAP Зенит 1 РГС']",
    "Полис страхование жизни Зенит РГС (КСЖ)": "//A[@class='PageRequestStep09__printLink'][text()='Полис КСЖ Зенит 1 РГС']",
    "Заявление на перевод денег страхование жизни Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег КСЖ Зенит РГС']",
    "Заявление на страхование Финкаско Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на страхование Финкаско Зенит РГС']",
    "Заявление на перевод денег ДМС Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег ДМС при ДТП Зенит РГС']",
    "Полис ДМС Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Полис ДМС при ДТП Зенит РГС']",
    "Полис ФинКаско Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Полис Финкаско РГС']",
    "Приложение №1 к полису Финкаско Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Приложение №1 к полису Финкаско Зенит РГС']",
    "Приложение №2 к полису Финкаско Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Приложение №2 к полису Финкаско Зенит РГС']",
    "Заявление на перевод денег ФинКаско Зенит РГС": "//A[@class='PageRequestStep09__printLink'][text()='Заявление на перевод денег Финкаско РГС']"
}

DOWNLOAD = {
    "ИУ": "(//INPUT[@class='BpUpload__input'])[1]",
    "График": "(//INPUT[@class='BpUpload__input'])[2]",
    "Анкета": "(//INPUT[@class='BpUpload__input'])[3]",
    "Договор залога": "(//INPUT[@class='BpUpload__input'])[4]",
    "Перевод за ТС": "(//INPUT[@class='BpUpload__input'])[5]",
    "Заявление на открытие счетов БИБ": "(//INPUT[@class='BpUpload__input'])[4]",
    "Зенит открытие счетов": "(//INPUT[@class='BpUpload__input'])[5]",
    "Зенит перевод за ТС": "(//INPUT[@class='BpUpload__input'])[6]",
    "ФинКаско Зенит": "(//INPUT[@class='BpUpload__input'])[7]",
    "Доп соглашение": "(//INPUT[@class='BpUpload__input'])[8]",
    "Доп соглашение Зенит": "(//INPUT[@class='BpUpload__input'])[7]"

}

CLIENT_ANSWERS = {
    'Ответы клиента': [([{'score': 0, 'answer': 'Да',
                          'question': 'Гульнара Ильдаровна , вы подавали сегодня Заявку на автокредит? ',
                          'question_code': '0101'}, {'score': 0, 'answer': '650000',
                                                     'question': 'Какая, приблизительно, стоимость авто, которую вы планируете приобрести?',
                                                     'question_code': '0200'}, {'score': 0, 'answer': '120000',
                                                                                'question': 'Назовите, пожалуйста, сумму первоначального взноса.',
                                                                                'question_code': '0202'},
                         {'score': 0, 'answer': '20000',
                          'question': 'Какая сумма ежемесячного платежа по кредиту будет для Вас комфортной?',
                          'question_code': '0203'}, {'score': 1, 'answer': '0',
                                                     'question': 'Назовите кол-во ваших действующих кредитов, не считая кредитные карты.',
                                                     'question_code': '0600'},
                         {'score': 0, 'answer': 'Да', 'question': 'Как называется компания, в которой Вы работаете?',
                          'question_code': '1300'}, {'score': 0, 'answer': 'ДМИТРОВСКОЕ Ш 81',
                                                     'question': 'По какому адресу находится фактическое место вашей работы (основной)?   ',
                                                     'question_code': '1302'},
                         {'score': 0, 'answer': '1-3 года', 'question': 'Как давно вы работаете в текущей компании?',
                          'question_code': '1304'}, {'score': 0, 'answer': '53',
                                                     'question': 'Как называется Должность, на которой вы работаете (по основному месту работы)?',
                                                     'question_code': '1306'},
                         {'score': 1, 'answer': 'Да', 'question': 'к предыдущему вопросу', 'question_code': '1307'},
                         {'score': 0, 'answer': '132000',
                          'question': 'Назовите сумму вашего среднемесячного дохода, получаемого по основному месту работы. Можно округлить до тысяч.',
                          'question_code': '1309'}, {'score': 1, 'answer': '5',
                                                     'question': 'У вас есть непосредственные подчиненные? Если да, то Сколько?',
                                                     'question_code': '1310'}, {'score': 0, 'answer': '(null)',
                                                                                'question': 'Верификатор, отметьте, если есть подозрение на мошенничество',
                                                                                'question_code': '1900'}],)]
}
CLIENT_SCOR = {
    'Скорбалл клиента': [({'count_score': 1, 'amount_score': 3, 'status_score': 0},)]
}

EMPLOYER_ANSWERS = {
    'Ответы работодателя':
        [([{'score': 3, 'answer': 'Да, работает',
            'question': 'Можете подтвердить, что в вашей компании работает Ханова Гульнара Ильдаровна ?',
            'question_code': '7101'}, {'score': 0, 'answer': '(null)', 'question': '!!!', 'question_code': '7102'},
           {'score': 0, 'answer': '(null)',
            'question': 'Можете подтвердить, что  Гульнара  Ильдаровна  работает в вашей компании более 3 месяцев?',
            'question_code': '7201'},
           {'score': 0, 'answer': 'Врач',
            'question': 'Скажите, пожалуйста, в какой должности работает   Гульнара  Ильдаровна ?',
            'question_code': '7500'}, {'score': 1, 'answer': 'Да', 'question': 'продолжение', 'question_code': '7501'},
           {'score': 1, 'answer': 'Да',
            'question': 'Скажите, фактическое место работы  Гульнара  Ильдаровна находится по адресу Респ Башкортостан, село Буздяк, ул Социалистическая, д 2 ?',
            'question_code': '7802'},
           {'score': 0, 'answer': '(null)', 'question': 'Свободный комментарий', 'question_code': '7900'}],)]
}
EMPLOYER_SCOR = {
    'Скорбалл работодателя': [({'count_score': 1, 'amount_score': 5, 'status_score': 0},)]
}

SPOUSE_ANSWERS = {
    'Ответ супруга':
        [([{'score': 0, 'answer': 'yes',
            'question': '(Вы звоните СУПРУГЕ(у) заемщика )  В наш Банк обратился  Ханова  Гульнара Ильдаровна, знаете такого человека?',
            'question_code': '2101'}, {'score': 1, 'answer': 'более 10 лет',
                                       'question': 'Как давно Гульнара Ильдаровна  работает на  текущем месте работы? ',
                                       'question_code': '2501'},
           {'score': 1, 'answer': 'yes', 'question': 'У Гульнара Ильдаровна  есть подчиненные?',
            'question_code': '2502'}, {'score': 0, 'answer': '1000000',
                                       'question': 'Назовите, пожалуйста, примерную стоимость авто, которое  Гульнара Ильдаровна планирует купить в кредит.',
                                       'question_code': '2600'}, {'score': 1, 'answer': '0',
                                                                  'question': 'Назовите, пожалуйста, количество действующих кредитов, не считая кредитных карт, у  Гульнара Ильдаровна',
                                                                  'question_code': '2601'},
           {'score': 0, 'answer': '(null)', 'question': 'Верификатор, отметьте, если есть подозрение на мошенничество',
            'question_code': '2900'},
           {'score': 0, 'answer': '(null)', 'question': 'Свободный комментарий', 'question_code': '2901'}],)]
}
SPOUSE_SCOR = {
    'Скорбалл супруга': [({'count_score': 0, 'amount_score': 3, 'status_score': 0},)]
}

USERS = {
    'пользователь1': 'verificationtestuser1',
    'пользователь2': 'VerificationTestUser2',
    'пользователь3': 'VerificationTestUser3',
    'пользователь4': 'VerificationTestUser4',
    'пользователь5': 'VerificationTestUser5',
    'пользователь6': 'VerificationTestUser6',
    'пользователь7': 'VerificationTestUser7',
    'пользователь8': 'VerificationTestUser8',
    'пользователь9': 'VerificationTestUser9',
    'пользователь10': 'VerificationTestUser10',
    'пользователь11': 'VerificationTestUser11',
    'пользователь12': 'VerificationTestUser12',
    'пользователь13': 'VerificationTestUser13',
    'пользователь14': 'VerificationTestUser14',
    'пользователь15': 'VerificationTestUser15',
    'пользователь16': 'VerificationTestUser16'
}

PASSWORDS = {
    'пароль1': '12345678',
    'пароль2': '12345678',
    'пароль3': '12345678',
    'пароль4': '12345678',
    'пароль5': '12345678',
    'пароль6': '12345678',
    'пароль7': '12345678',
    'пароль8': '12345678',
    'пароль9': '12345678',
    'пароль10': '12345678',
    'пароль11': '12345678',
    'пароль12': '12345678',
    'пароль13': '12345678',
    'пароль14': '12345678',
    'пароль15': '12345678',
    'пароль16': '12345678'
}

ITERATIONS = {
    "ИУ": 7,
    "График платежей": 3,
    "АЗ и согласие": 4,
    "Договор залога": 7,
    "Открытие счетов": 1,
    "Перевод за ТС": 1,
    "Доп соглашение": 1,
    "Заявление перевод КСЖ РГС": 1,
    "Прил 1 КСЖ график РГС": 3,
    "Полис КСЖ РГС": 2,
    "Памятка КСЖ РГС": 1,
    "Полис ГАП РГС": 1,
    "Заявление перевод ГАП РГС": 1,
    "Заявление перевод КАСКО партн": 1,
    "Приложение 2 к полису ГАП РГС": 1,
    "Заявление на страхование ГАП РГС": 1,

}

REASONS = {
    'Номер не существует или нет гудков': "//*[text()[contains(.,'Номер не существует или нет гудков')]]",
    'Отказ разговаривать': "//*[text()[contains(.,'Отказ разговаривать')]]",
    'Номер принадлежит другому человеку': "//*[text()[contains(.,'Номер принадлежит другому человеку')]]",
    'Номер заблокирован': "//*[text()[contains(.,'Номер заблокирован')]]",
    'Абонент сбрасывает или не отвечает': "//*[text()[contains(.,'Абонент сбрасывает или не отвечает')]]",
    'Абонент недоступен': "//*[text()[contains(.,'Абонент недоступен')]]"
}

CREDIT_HISTORY = {
    'НБКИ': [([{'code': 'E41', 'final': True, 'cascade': False, 'message': 'НБКИ_антифрод_балл', 'green_zone': False,
                'pre_decline': False}],)],
    'ФССП': [([{'code': 'C23', 'final': True, 'cascade': False, 'message': 'ФССП_кредитная_задолженность_безСуммы',
                'green_zone': False, 'pre_decline': False}],)],
    'ОКБ': [([{'code': 'C13', 'final': True, 'cascade': False, 'message': 'ОКБ_скорбалл', 'green_zone': False,
               'pre_decline': False}],)],
    'ЭКВИФАКС': [([{'code': 'C12', 'final': True, 'cascade': False, 'message': 'Эквифакс_скорбалл', 'green_zone': False,
                    'pre_decline': False}],)]
}

CALL_STRATEGY = {
    'A0': [('A0',)],
    'A6': [('A6',)]
}

DADATA = {
    'street': [('Куликовская',)],
    'flat': [('326',)]
}

DECLINE = {
    'ФИО': 'Поля «ФИО» / «Дата рождения» / «Место рождения» нечитаемы.',
    'ВУ': 'Документ нечёткий или не читается из-за дефектов.',
    'Согласие': 'Документ нечёткий или не читается из-за дефектов.',
    'Пасспорт': 'Серия и номер паспорта не совпадают с введенными значениями.',
    'Выдан': 'Поля «Кем выдан» / «Дата выдачи» / «Код подразделения» нечитаемы.',
    'Адрес': 'Поле «Адрес» нечитаемо.'
}

USERS2 = {
    'user10': {
        'login': 'VerificationTestUser10',
        'pass': '3edc$RFV'
    },
    'user1': {
        'login': 'verificationtestuser1',
        'pass': '12345678'
    },
    'user2': {
        'login': 'VerificationTestUser2',
        'pass': '12345678'
    },
    'user3': {
        'login': 'VerificationTestUser3',
        'pass': '12345678'
    },
    'user4': {
        'login': 'VerificationTestUser4',
        'pass': '12345678'
    },
    'user5': {
        'login': 'VerificationTestUser5',
        'pass': '12345678'
    },
    'user6': {
        'login': 'VerificationTestUser6',
        'pass': '3edc$RFV'
    },
    'user7': {
        'login': 'VerificationTestUser7',
        'pass': '3edc$RFV'
    },
    'user8': {
        'login': 'VerificationTestUser8',
        'pass': '3edc$RFV'
    },
    'user9': {
        'login': 'VerificationTestUser9',
        'pass': '3edc$RFV'
    },
}

LIGHTS = {
    'Красный': "//BUTTON[@class='VisualAssessment__button -red -selected-no']",
    'Желтый': "//BUTTON[@class='VisualAssessment__button -yellow -selected-no']"
}

VA = {
    "1П": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='1П']",
    "2М": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='2М']",
    "3Б": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='3Б']",
    "4С": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='4С']",
    "5З": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='5З']",
    "6ПК": "//BUTTON[@class='VisualAssessment__mark -selected-no'][text()='6ПК']"
}

# person = Person('ru')
# full_name = person.full_name(Gender.MALE)
# CLIENTS['fake']['firstName'] = full_name.split(' ')[0]
# CLIENTS['fake']['lastName'] = full_name.split(' ')[1]
# _ = RussiaSpecProvider()
# CLIENTS['fake']['secondName'] = _.patronymic(gender=Gender.MALE)
# CLIENTS['fake']['passport'] = person.identifier(mask='1111-######')
# CLIENTS['fake']['birthday'] = person.identifier(mask='1#.01.198#')
# CLIENTS['fake']['divisionCode'] = person.identifier(mask='###-###')
# CLIENTS['fake']['issuedAt'] = person.identifier(mask='2#.01.2018')
# # Б/У
# CLIENTS['fake']['license-issuedAt'] = '28.12.2016'
# CLIENTS['fake']['license-number'] = '7830172235'
# CLIENTS['fake']['pts-number'] = '77ун945574'
# CLIENTS['fake']['pts-vin'] = 'jtdkn3duxa0026650'
# CLIENTS['fake']['pts-issuedAt'] = '16.05.2012'
# CLIENTS['fake']['pts-tsDate'] = '1934'
# CLIENTS['fake']['pts-tsColor'] = 'серебристый'
# CLIENTS['fake']['pts-shassy'] = 'отсутствует'
# CLIENTS['fake']['pts-model'] = '2zr4352878'
