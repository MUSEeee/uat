# установка базового образа (host OS)
FROM python:3.8

# установка рабочей директории в контейнере
WORKDIR /project/pythonProject

# [in the / folder]
COPY . .
#RUN git clone https://gitlab.com/MUSEeee/uat.git /project

# установка зависимостей
RUN pip install behave==1.2.6
RUN pip install selenium
RUN pip install fuzzywuzzy
RUN pip install mimesis
RUN pip install numpy
RUN pip install requests
RUN pip install dadata
# set display port to avoid crash
ENV DISPLAY=:99

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

## install chromedriver
#RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/91.0.4472.19/chromedriver_linux64.zip && unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# команда, выполняемая при запуске контейнера
RUN chmod a+x /usr/local/bin/
CMD curl https://partnerka-front-internal-st1.auto-caft-st.dc-yc.b-pl.pro/
CMD cd /project
CMD behave -i step-1.feature




#docker pull 555338/kotov
#docker run -v /var/www:/var/www -p 80:80 -t kotov - запуск контейнера
#docker build -t kotov ~/PATH_TO_DOCKERFILE_DIR --no-cache - создание образа
#docker exec -i -t reverent_fermi bash - подключение к контейнеру
